package be.mustafa.spring.rest.services;

import be.mustafa.spring.rest.messages.Message;
import be.mustafa.spring.rest.messages.MessageList;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;

public interface MessageRestService {
    ResponseEntity<Message> getMessage(int id);
    ResponseEntity<MessageList> getMessages();
    ResponseEntity addMessage(Message message, HttpServletRequest request);
    ResponseEntity updateMessage(int id, Message message);
    ResponseEntity<Message> patchMessage(int id, Message message);
    ResponseEntity<?> deleteMessage(int id);
}
