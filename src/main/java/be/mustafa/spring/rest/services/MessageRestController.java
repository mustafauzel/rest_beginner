package be.mustafa.spring.rest.services;

import be.mustafa.spring.rest.messages.Message;
import be.mustafa.spring.rest.messages.MessageList;
import be.mustafa.spring.rest.messages.MessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.net.URI;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/**/messages")
public class MessageRestController implements MessageRestService{
    @Autowired
    private MessageRepository repo;

    @Override
    @GetMapping(value = "{id}",
            produces = {MediaType.APPLICATION_JSON_UTF8_VALUE,
            MediaType.APPLICATION_XML_VALUE})
    public ResponseEntity<Message> getMessage( @PathVariable("id") int id) {
        Message message = repo.getMessageById(id);
        if(message != null){
            return new ResponseEntity(message, HttpStatus.OK);
        }else {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    @GetMapping(
            produces = {MediaType.APPLICATION_XML_VALUE,
            MediaType.APPLICATION_JSON_UTF8_VALUE}
    )
    public ResponseEntity<MessageList> getMessages(){
        List<Message> messages = repo.getAllMessages();
        MessageList list = new MessageList(messages);
        if(messages != null) {
            System.out.println(list.getMessages());
            return new ResponseEntity<>(list, HttpStatus.OK);
        }else{
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    @PostMapping(
            consumes = {MediaType.APPLICATION_XML_VALUE,
            MediaType.APPLICATION_JSON_UTF8_VALUE}
    )
    public ResponseEntity addMessage(@Valid @RequestBody Message message, HttpServletRequest request) {
        if(message.getId() != 0){
            return ResponseEntity.badRequest().build();
        }
        message = repo.createMessage(message);
        URI uri = URI.create(request.getRequestURL() + "/" + message.getId());
        return ResponseEntity.created(uri).build();
    }

    @Override
    @PutMapping(value = "{id:^\\d+$}",
    consumes = {MediaType.APPLICATION_XML_VALUE,
    MediaType.APPLICATION_JSON_UTF8_VALUE})
    public ResponseEntity updateMessage(@PathVariable("id") int id, @Valid @RequestBody Message message){
        if(message.getId() != id){
            return ResponseEntity.badRequest().build();
        }
        message = repo.updateMessage(message);
        return ResponseEntity.ok().build();
    }

    @Override
    @PatchMapping(value = "{id:^\\d+$}",
    consumes = {MediaType.APPLICATION_JSON_UTF8_VALUE, MediaType.APPLICATION_XML_VALUE},
    produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_UTF8_VALUE})
    public ResponseEntity<Message> patchMessage(
            @PathVariable("id") int id, @RequestBody Message patchMessage){
        Message message = repo.getMessageById(id);
        if(message == null){
            return ResponseEntity.notFound().build();
        }

        if((patchMessage.getId() != 0) && (patchMessage.getId() != id)){
            return ResponseEntity.badRequest().build();
        }

        if(patchMessage.getAuthor() != null){
            message.setAuthor(patchMessage.getAuthor());
        }

        if(patchMessage.getText() != null){
            message.setText(patchMessage.getText());
        }
        message = repo.updateMessage(message);
        System.out.println(message);
        return ResponseEntity.ok(message);
    }

    @Override
    @DeleteMapping(value = "{id:^\\d+$}")
    public ResponseEntity<?> deleteMessage(@PathVariable("id") int id) {
        repo.deleteMessage(id);
        return ResponseEntity.ok().build();
    }

    @ExceptionHandler
    public ResponseEntity<String> handleException(Exception ex){
        return new ResponseEntity<>(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
