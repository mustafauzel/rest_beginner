package be.mustafa.spring.rest.messages;

import java.util.List;

public interface MessageRepository {
    Message getMessageById(int id);
    List<Message> getAllMessages();
    List<Message> getMessagesByAuthor(String author);
    Message createMessage(Message message);
    Message updateMessage(Message message);
    void deleteMessage(int id);
}
