open module be.mustafa.spring.rest{
    requires spring.context;
    requires spring.core;
    requires spring.beans;
    requires spring.boot;
    requires spring.boot.autoconfigure;
    requires java.sql;

    //REST
    requires spring.web;
    //JEE annotations
    requires java.annotation;
    //XML binding
    requires java.xml.bind;
    requires com.sun.xml.bind;
    //JSON binding
    requires jackson.annotations;
    //TOMCAT JEE classes
    requires tomcat.embed.core;
    //Bean Validation API
    requires java.validation;
}
