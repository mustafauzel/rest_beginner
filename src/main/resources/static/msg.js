function getMessage() {
    const messageId = document.getElementById("messageId").value;
    const xmlrequest = new XMLHttpRequest();
    xmlrequest.onload = onLoad;
    xmlrequest.open("GET", "messages/" + messageId, true);
    xmlrequest.setRequestHeader("Accept", "application/json");
    //xmlrequest.setRequestHeader("Authorization", "Basic" + btoa("homer:password"));
    xmlrequest.send(null);
}

function onLoad() {
    const messageField = document.getElementById("messageField");
    let messageText = "";
    switch (this.status) {
        case 200:
            const message = JSON.parse(this.responseText);
            messageText = message.Author + " : " + message.Text;
            break;
        case 404:
            messageText = "Message not found";
            break;
        default:
            messageText = "Error. Status: " + response.status;
    }
    messageField.innerText = messageText;
}

function init(){
    const button = document.getElementById("getButton");
    button.addEventListener("click", getMessage);
}

window.addEventListener("load", init);
